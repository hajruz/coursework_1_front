export default class AlertSystem {
  static showSuccess (text) {
    let alertPreviousItem = document.getElementById('ec-alert')
    if (alertPreviousItem !== null &&
      alertPreviousItem !== undefined &&
      alertPreviousItem.getAttribute('class').includes('alert-success')) {
      document.getElementById('ec-alert-text').innerText = text
      return
    }

    if (alertPreviousItem !== null &&
      alertPreviousItem !== undefined) {
      alertPreviousItem.setAttribute('class', 'alert alert-success mb-0 ec-alert')
      document.getElementById('ec-alert-text').innerText = text
      document.getElementById('ec-alert-header').innerText = 'Успешно!'
      return
    }

    let alertItem = document.createElement('div')
    alertItem.setAttribute('id', 'ec-alert')
    alertItem.setAttribute('class', 'alert alert-success mb-0 ec-alert')
    alertItem.setAttribute('role', 'alert')

    let alertHeader = document.createElement('h4')
    alertHeader.setAttribute('id', 'ec-alert-header')
    alertHeader.setAttribute('class', 'alert-heading')
    alertHeader.innerText = 'Успешно!'
    let alertText = document.createElement('p')
    alertText.setAttribute('id', 'ec-alert-text')
    alertText.innerText = text
    alertItem.appendChild(alertHeader)
    alertItem.appendChild(alertText)
    let closeButton = document.createElement('button')
    closeButton.setAttribute('class', 'btn-alert')
    closeButton.onclick = this.removeAlert
    closeButton.innerText = 'Закрыть'
    alertItem.appendChild(closeButton)
    this.addAlert(alertItem)
  }
  static showError (text) {
    let alertPreviousItem = document.getElementById('ec-alert')
    if (alertPreviousItem !== null &&
      alertPreviousItem !== undefined &&
      alertPreviousItem.getAttribute('class').includes('alert-danger')) {
      document.getElementById('ec-alert-text').innerText = text
      return
    }

    if (alertPreviousItem !== null &&
      alertPreviousItem !== undefined) {
      alertPreviousItem.setAttribute('class', 'alert alert-danger mb-0 ec-alert')
      document.getElementById('ec-alert-text').innerText = text
      document.getElementById('ec-alert-header').innerText = 'Упс... Что-то пошло не так!'
      return
    }

    let alertItem = document.createElement('div')
    alertItem.setAttribute('id', 'ec-alert')
    alertItem.setAttribute('class', 'alert alert-danger mb-0 ec-alert')
    alertItem.setAttribute('role', 'alert')

    let alertHeader = document.createElement('h4')
    alertHeader.setAttribute('id', 'ec-alert-header')
    alertHeader.setAttribute('class', 'alert-heading')
    alertHeader.innerText = 'Упс... Что-то пошло не так!'
    let alertText = document.createElement('p')
    alertText.setAttribute('id', 'ec-alert-text')
    alertText.innerText = text
    alertItem.appendChild(alertHeader)
    alertItem.appendChild(alertText)
    let closeButton = document.createElement('button')
    closeButton.setAttribute('class', 'btn-alert')
    closeButton.onclick = this.removeAlert
    closeButton.innerText = 'Закрыть'
    alertItem.appendChild(closeButton)
    this.addAlert(alertItem)
  }

  static addAlert (element) {
    document.getElementById('ec-info').appendChild(element)
  }

  static removeAlert () {
    let alertItem = document.getElementById('ec-alert')
    alertItem.className += ' ec-alert-remove'
    setTimeout(() => {
      document.getElementById('ec-info').removeChild(alertItem)
    }, 750)
  }
}
