import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Products from '@/components/Products'
import Product from '@/components/Product'
import Cart from '@/components/Cart'
import Orders from '@/components/Orders'
import About from '@/components/About'
import Contacts from '@/components/Contacts'
import PageNotFound from '@/components/PageNotFound'
import Login from '@/components/Login'
import Admin from '@/components/admin/Admin'
import AdminUsers from '@/components/admin/Users'
import AdminProducts from '@/components/admin/Products'
import Registration from '@/components/Registration'
import Authentication from '@/components/Authentication'
import ProfileConfirm from '@/components/ProfileConfirm'
import ResetPassword from '@/components/ResetPassword'
import ResetPasswordForm from '@/components/ResetPasswordForm'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome,
      meta: {
        title: 'Главная'
      }
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      meta: {
        title: 'Админка',
        authority: 'ADMIN'
      }
    },
    {
      path: '/admin/users',
      name: 'AdminUsers',
      component: AdminUsers,
      meta: {
        title: 'Админка',
        authority: 'ADMIN'
      }
    },
    {
      path: '/admin/products',
      name: 'AdminProducts',
      component: AdminProducts,
      meta: {
        title: 'Админка',
        authority: 'ADMIN'
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        title: 'Вход',
        authority: 'ANONYMOUS'
      }
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration,
      meta: {
        title: 'Регистрация',
        authority: 'ANONYMOUS'
      }
    },
    {
      path: '/products',
      name: 'Products',
      component: Products,
      meta: {
        title: 'Товары'
      }
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
      meta: {
        title: 'Корзина',
        authority: 'USER'
      }
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders,
      meta: {
        title: 'Покупки',
        authority: 'USER'
      }
    },
    {
      path: '/product/:id',
      name: 'Product',
      component: Product,
      meta: {
        title: 'Товар'
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        title: 'О компании'
      }
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts,
      meta: {
        title: 'Наши контакты'
      }
    },
    {
      path: '/profile/confirm/:code',
      name: 'ProfileConfirm',
      component: ProfileConfirm,
      meta: {title: 'Подтверждение аккаунта'}
    },
    {
      path: '/profile/reset',
      name: 'ResetPassword',
      component: ResetPassword,
      meta: {
        title: 'Сброс пароля',
        authority: 'ANONYMOUS'
      }
    },
    {
      path: '/reset/password/:code',
      name: 'ResetPasswordForm',
      component: ResetPasswordForm,
      meta: {
        title: 'Сброс пароля',
        authority: 'ANONYMOUS'
      }
    },
    {
      path: '*',
      name: 'PageNotFound',
      component: PageNotFound,
      meta: {
        title: 'Страница не найдена'
      }
    }
  ]
})

export default router

router.beforeEach((to, from, next) => {
  document.title = 'Jewelry'
  if (to.meta.authority !== null && to.meta.authority !== undefined) {
    Authentication.methods.hasAuthority(to.meta.authority, (state) => {
      if (state) {
        document.title = to.meta.title
        next()
      } else if (to.meta.authority === 'ANONYMOUS') {
        document.location = '/'
        Authentication.methods.authorize()
      } else {
        next('/login')
      }
    })
  } else {
    document.title = to.meta.title
    next()
  }
})
